//Zach Coenen
//Forked from Byron

#include <iostream>
#include <conio.h>

using namespace std;

enum Suit 
{
    SPADES,
    DIAMONDS,
    CLUBS,
    HEARTS
};

enum Rank
{
    TWO = 2,
    THREE = 3,
    FOUR = 4,
    FIVE = 5,
    SIX = 6,
    SEVEN = 7,
    EIGHT = 8,
    NINE = 9,
    TEN = 10,
    JACK = 11,
    QUEEN = 12,
    KING = 13,
    ACE = 14
};

struct Card
{
    Suit suit;
    Rank rank;
};


void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
    Card C1;
    Card C2;
    Card High;


    C1.suit = SPADES;
    C1.rank = FOUR;

    C2.suit = HEARTS;
    C2.rank = QUEEN;


    High = HighCard(C1, C2);
    PrintCard(High);




    _getch();
    return 0;
}



void PrintCard(Card card)
{
    string CRank;
    string CSuit;

    //Convert Rank Enum to word representation
    switch (card.rank)
    {
    case 2:
        CRank = "Two";
        break;
    case 3:
        CRank = "Three";
        break;
    case 4:
        CRank = "Four";
        break;
    case 5:
        CRank = "Five";
        break;
    case 6:
        CRank = "Six";
        break;
    case 7:
        CRank = "Seven";
        break;
    case 8:
        CRank = "Eight";
        break;
    case 9:
        CRank = "Nine";
        break;
    case 10:
        CRank = "Ten";
        break;
    case 11:
        CRank = "Jack";
        break;
    case 12:
        CRank = "Queen";
        break;
    case 13:
        CRank = "King";
        break;
    case 14:
        CRank = "Ace";
        break;
    default:
        cout << "Larry?";
        break;
    }


    //Convert Suit Enum to word representation
    switch (card.suit)
    {
    case 0:
        CSuit = "Spades";
        break;
    case 1:
        CSuit = "Diamonds";
        break;
    case 2:
        CSuit = "Clubs";
        break;
    case 3:
        CSuit = "Hearts";
        break;


    default:
        break;
    }


    std::cout << "The " << CRank << " of " << CSuit;
}



Card HighCard(Card card1, Card card2)
{
    //check rank for high card, if equal then check suit. If suit is the same return card 2 as default
    if (card1.rank > card2.rank)
    {
        return card1;
    }
    else if (card2.rank > card1.rank)
    {
        return card2;
    }
    else if (card1.rank == card2.rank)
    {
        if (card1.suit > card2.suit)
        {
            return card1;
        }
        else
        {
            return card2;
        }
    }


}